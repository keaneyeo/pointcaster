﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerLoader : MonoBehaviour
{
    [SerializeField] GameObject _controllerPrefab;
    static OverallManager _overallManager = null;
    void Awake()
    {
        if (_overallManager == null)
            _overallManager = Instantiate(_controllerPrefab).GetComponent<OverallManager>();
    }
}
