﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    int _enemy_id;
    int _baseHealth;
    int _attack;
    int _enemy_type;
    int _currentHealth;
    public int CurrentHealth { get { return _currentHealth; } }
    public int MaxHealth { get { return _baseHealth; } }
    Animator _anim = null;
    public bool PlayingAnimation = false;
    public bool Dead = false;

    public bool Init(EnemyData ed, int stage)
    {
        _enemy_id = ed.enemy_id;
        _baseHealth = ed.health;
        _enemy_type = ed.enemy_type;
        float modifier = (_enemy_type == 0) ? 0.25f : 0.30f;
        _attack = Mathf.FloorToInt(ed.attack + ((0.25f * ed.attack) * stage));
        int random = Random.Range(0, 3);
        Sprite sprite = (_enemy_type == 0) ? EnemyManager.Instance.NormalEnemySprites[random] : EnemyManager.Instance.BossSprites[random];
        this.GetComponent<SpriteRenderer>().sprite = sprite;
        _currentHealth = _baseHealth;
        _anim = this.GetComponent<Animator>();
        _anim.runtimeAnimatorController = EnemyManager.Instance.NormalEnemyAnims[random];
        return true;
    }

    public int GetAttackValue()
    {
        return _attack;
    }

    /// <summary>
    /// Check if enemy dead, If true, enemy dead
    /// </summary>
    /// <param name="damage">damage taken by enemy</param>
    /// <returns></returns>
    public bool CheckDead(int damage)
    {
        _currentHealth -= damage;
        if (_currentHealth <= 0)
            return true;
        else
            return false;
    }

    public void SetDeathAnim()
    {
        if (!Dead)
        {
            _anim.SetTrigger("dead");
            PlayingAnimation = true;
            Dead = true;
        }
    }
}
