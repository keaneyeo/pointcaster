﻿public static class Constants
{
    public const string ConnectionString = "URI=file:Assets/StreamingAssets/rookiesmain.db";
    public const string AddWaveDataEvent = "AddWaveDataEvent";
    public const string ClosePanel = "ClosePanels";
    public const string UpdateEquip = "UpdateBar";
    public const string DisplayDesc = "Display";
    public const string OnPlayerHit = "OnPlayerHit";
    public const string Lose = "Lose";
    #region GameStateEvents
    public const string OnGameStateChange = "OnGameStateChange";
    public const string OnRuneStartDraw = "OnRuneStartDraw";
    public const string OnRuneDrawnComplete = "OnRuneDrawnComplete";
    #endregion
}
