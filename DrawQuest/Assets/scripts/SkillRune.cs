﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillRune : MonoBehaviour
{
    public SkillPattern _pattern = SkillPattern.none;
    Image _img;
    Button _btn;

    void Start()
    {
        _btn = this.gameObject.AddComponent<Button>();
        _btn.onClick.AddListener(DisplayDetails);
        _btn.onClick.AddListener(() => { AudioManager.Instance.PlaySound(SoundType.btn); });
    }

    public void SetVisuals(int pattern)
    {
        Sprite[] _sprites = Resources.LoadAll<Sprite>("Sprites/skills");
        _img = GetComponent<Image>();
        _pattern = (SkillPattern)pattern;
        _img.sprite = _sprites[pattern];
    }

    void DisplayDetails()
    {
        Core.BroadcastEvent(Constants.DisplayDesc, this, (int)_pattern);
    }

    void OnDestroy()
    {
        _btn.onClick.RemoveAllListeners();
    }
}

public enum SkillPattern
{
    SA1,
    SA2,
    SD1,
    SD2,
    wrong,
    none
}
