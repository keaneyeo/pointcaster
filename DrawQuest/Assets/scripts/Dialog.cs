﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Dialog : MonoBehaviour
{
    static Dialog _instance;
    public static Dialog Instance { get { return _instance; } }
    private void Awake()
    {
        _instance = this;
    }
    [SerializeField] Text _dialogMainText;
    [SerializeField] Button _confirmBtn;
    CanvasGroup _cg;

	void Start ()
    {
        _cg = GetComponent<CanvasGroup>();
        Hide();
        _confirmBtn.onClick.AddListener(Hide);
	}
    void OnDestroy()
    {
        _instance = null;
        _confirmBtn.onClick.RemoveAllListeners();
    }

    public void Show(UnityAction action)
    {
        _confirmBtn.onClick.AddListener(action);
        Show();
    }
    void Show()
    {
        _cg.alpha = 1;
        _cg.interactable = true;
        _cg.blocksRaycasts = true;
    }
    void Hide()
    {
        _cg.alpha = 0;
        _cg.interactable = false;
        _cg.blocksRaycasts = false;
    }
}
