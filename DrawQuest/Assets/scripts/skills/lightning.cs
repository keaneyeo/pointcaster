﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightning : MonoBehaviour
{
    float _timer = 0.5f;
    
    void Start ()
    {
        AudioManager.Instance.PlaySound(SoundType.lightning);
	}
	
	
	void Update ()
    {
	    if( _timer > 0)
        {
            _timer -= Time.deltaTime;
            return;
        }
        Destroy(this.gameObject);
	}
}
