﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : MonoBehaviour
{
    public Vector2 speed;
    Rigidbody2D rb;
    public float HealTime;
    // Use this for initialization

    void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = speed;
        Destroy(gameObject, HealTime);
        AudioManager.Instance.PlaySound(SoundType.heal);

	}
	
	// Update is called once per frame
	void Update ()
    {
        rb.velocity = speed;		
	}
}
