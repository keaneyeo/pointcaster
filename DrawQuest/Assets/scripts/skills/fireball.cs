﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    #region Instance
    static Fireball _instance;
    public static Fireball Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion

    //public GameObject player;
    public Vector2 speed;
    Rigidbody2D rb;

    //public float destroyTime;

    private void Awake()
    {
        _instance = this;
    }

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = speed;
        AudioManager.Instance.PlaySound(SoundType.attack);
        //Destroy(gameObject, destroyTime);

    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = speed;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            Destroy(gameObject);
        }

    }

    private void OnDestroy()
    {
        _instance = null;
    }
}
