﻿using UnityEngine;
using UnityEngine.UI;

public class CreditsPanel : MonoBehaviour
{
    [SerializeField] Button _closeBtn;

    void Start()
    {
        _closeBtn.onClick.AddListener(() => { Destroy(this.gameObject); });
	}

    private void OnDestroy()
    {
        _closeBtn.onClick.RemoveAllListeners();
    }
}
