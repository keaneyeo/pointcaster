﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhasePanel : MonoBehaviour
{
    [SerializeField] Image _image;
    [SerializeField] Sprite[] _adimages;
    Animator _anim;
    public bool Active;
	void Start ()
    {
        _anim = this.GetComponent<Animator>();
	}
	
	public bool Init(int attackdefend, Color color)
    {
        //_image
        _image.sprite = _adimages[attackdefend];
        _image.color = color;
        Active = true;
        return true;
    }
}
