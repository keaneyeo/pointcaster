﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPanel : MonoBehaviour
{
    //[SerializeField]
    //Toggle _masterToggle;
    [SerializeField]
    Toggle _BGMToggle;
    [SerializeField]
    Toggle _SFXToggle;
    //[SerializeField]
    //Slider _masterSlider;
    [SerializeField]
    Slider _BGMSlider;
    [SerializeField]
    Slider _SFXSlider;
    [SerializeField]
    Button _saveBtn;
    [SerializeField]
    Button _cancelBtn;

    void Start()
    {
        if(SetSettings())
        {
            //_masterToggle.onValueChanged.AddListener((bool a) => { AudioSettings.Master = _masterToggle.isOn; });
            _BGMToggle.onValueChanged.AddListener((bool b) => { AudioSettings.BGM = _BGMToggle.isOn; });
            _SFXToggle.onValueChanged.AddListener((bool c) => { AudioSettings.SFX = _SFXToggle.isOn; });
            //_masterSlider.onValueChanged.AddListener((float a) => { AudioSettings.MasterVol = _masterSlider.value; });
            _BGMSlider.onValueChanged.AddListener((float a) => { AudioSettings.BGMVol = _BGMSlider.value; });
            _SFXSlider.onValueChanged.AddListener((float a) => { AudioSettings.SFXVol = _SFXSlider.value; });
        }
        _saveBtn.onClick.AddListener(Save);
        _cancelBtn.onClick.AddListener(Close);
    }

    bool SetSettings()
    {
        //_masterToggle.isOn = AudioSettings.Master;
        _BGMToggle.isOn = AudioSettings.BGM;
        _SFXToggle.isOn = AudioSettings.SFX;
        //_masterSlider.value = AudioSettings.MasterVol;
        _BGMSlider.value = AudioSettings.BGMVol;
        _SFXSlider.value = AudioSettings.SFXVol;
        return true;
    }

    void WriteDictionary()
    {
        //ConfigParser.SettingsDictionary["master"] = AudioSettings.Master.ToString().ToLower();
        ConfigParser.SettingsDictionary["bgm"] = AudioSettings.BGM.ToString().ToLower();
        ConfigParser.SettingsDictionary["sfx"] = AudioSettings.SFX.ToString().ToLower();
        //ConfigParser.SettingsDictionary["mastervol"] = AudioSettings.MasterVol.ToString();
        ConfigParser.SettingsDictionary["bgmvol"] = AudioSettings.BGMVol.ToString();
        ConfigParser.SettingsDictionary["sfxvol"] = AudioSettings.SFXVol.ToString();
    }

    void OnDestroy()
    {
        //_masterToggle.onValueChanged.RemoveAllListeners();
        _BGMToggle.onValueChanged.RemoveAllListeners();
        _SFXToggle.onValueChanged.RemoveAllListeners();
        //_masterSlider.onValueChanged.RemoveAllListeners();
        _BGMSlider.onValueChanged.RemoveAllListeners();
        _SFXSlider.onValueChanged.RemoveAllListeners();
        _saveBtn.onClick.RemoveAllListeners();
        _cancelBtn.onClick.RemoveAllListeners();
    }

    void Save()
    {
        WriteDictionary();
        ConfigParser.WriteConfig();
        AudioManager.Instance.UpdateMixer();
        Close();
    }

    void Close()
    {
        Core.BroadcastEvent(Constants.ClosePanel, this);
    }
}
