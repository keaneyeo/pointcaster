﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventPanel : MonoBehaviour
{
    List<Skill> _atkSkills = new List<Skill>();
    List<Skill> _defSkills = new List<Skill>();
    [SerializeField]
    Transform _atkSection;
    [SerializeField]
    Transform _defSection;
    [SerializeField]
    GameObject _rune;
    [SerializeField]
    Button _closeBtn;
    [SerializeField]
    Image[] _slots;

    void Start ()
    {
        if(LoadSkills())
        {
            EquipBar();
        }
        _closeBtn.onClick.AddListener(() => { Core.BroadcastEvent(Constants.ClosePanel, this); });
        Core.SubscribeEvent(Constants.UpdateEquip, UpdateBar);
	}
	
    bool LoadSkills()
    {
        for (int i = 0; i < DataContainer.Skills.Length; i++)
        {
            int x = DataContainer.Skills[i].type;
            switch(x)
            {
                case 0:
                    _atkSkills.Add(DataContainer.Skills[i]);
                    break;
                case 1:
                    _defSkills.Add(DataContainer.Skills[i]);
                    break;
            }
        }

        foreach (Skill s in _atkSkills)
        {
            GameObject o = Instantiate(_rune, _atkSection, false);
            o.GetComponent<SkillRune>().SetVisuals(s.skill_id - 1);
        }

        foreach (Skill s in _defSkills)
        {
            GameObject o = Instantiate(_rune, _defSection, false);
            o.GetComponent<SkillRune>().SetVisuals(s.skill_id - 1);
        }
        return true;
    }

    void UpdateBar(object sender, object[] args)
    {
        EquipBar();
    }

    void EquipBar()
    {
        DataContainer.GetInventory();
        Sprite[] _sprites = Resources.LoadAll<Sprite>("Sprites/skills");
        for (int i = 0; i < _slots.Length; i++)
        {
            for (int j = 0; j < DataContainer.Invent.Length; j++)
            {
                if (DataContainer.Invent[j].skill_id == 0)
                    _slots[i].color = Vector4.zero;
                else if (DataContainer.Invent[j].inventory_id - 1 == i)
                    _slots[i].sprite = _sprites[DataContainer.Invent[j].skill_id - 1];
            }
        }
    }

    void OnDestroy()
    {
        _closeBtn.onClick.RemoveAllListeners();
        Core.UnsubscribeEvent(Constants.UpdateEquip, UpdateBar);
    }
}
