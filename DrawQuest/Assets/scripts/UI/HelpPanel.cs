﻿using UnityEngine;
using UnityEngine.UI;

public class HelpPanel : MonoBehaviour
{
    [SerializeField] ToggleGroup _toggleGroup;
    [SerializeField] Toggle[] _toggles;
    [SerializeField] GameObject[] _pages;
    [SerializeField] Button _close;
	void Start ()
    {
        for (int i = 0; i < _toggles.Length; i++)
        {
            int k = i;
            _toggles[k].onValueChanged.AddListener(
                (ison) =>
            {
                _pages[k].SetActive(ison);
                AudioManager.Instance.PlaySound(SoundType.btn);
            });
        }
        _close.onClick.AddListener(() => { Destroy(this.gameObject); });
    }
}
