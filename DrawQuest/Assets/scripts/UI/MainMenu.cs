﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    Button _shopBtn;
    [SerializeField]
    Button _helpBtn;
    [SerializeField]
    Button _inventBtn;
    [SerializeField]
    Button _startBtn;
    [SerializeField]
    Button _settingBtn;
    [SerializeField]
    Button _quitBtn;
    [SerializeField]
    Button _creditsBtn;

    private void Start()
    {
        _inventBtn.onClick.AddListener(() => { UIManager.Instance.OpenInvent(); });
        _startBtn.onClick.AddListener(() => { UIManager.Instance.GameStartStop(2); });
        _settingBtn.onClick.AddListener(() => { UIManager.Instance.OpenSettings(); });
        _quitBtn.onClick.AddListener(() => { UIManager.Instance.OpenDialog(); });
        _helpBtn.onClick.AddListener(() => { UIManager.Instance.OpenHelp(); });
        _creditsBtn.onClick.AddListener(() => { UIManager.Instance.OpenCredits(); });
    }

    private void OnDestroy()
    {
        _inventBtn.onClick.RemoveAllListeners();
        _startBtn.onClick.RemoveAllListeners();
        _settingBtn.onClick.RemoveAllListeners();
        _quitBtn.onClick.RemoveAllListeners();
        _helpBtn.onClick.RemoveAllListeners();
        _creditsBtn.onClick.RemoveAllListeners();
    }
}
