﻿using UnityEngine;
using UnityEngine.UI;

public class GenericButton : MonoBehaviour
{
    Button _button = null;
	void Start ()
    {
        _button = this.GetComponent<Button>();
        if (_button != null)
            _button.onClick.AddListener(() => { AudioManager.Instance.PlaySound(SoundType.btn); });
	}

    private void OnDestroy()
    {
        _button.onClick.RemoveAllListeners();
    }

}
