﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HurtFlash : MonoBehaviour
{
    public float TransitionTime = 2f;
    float _CurrentTransitionTime = 0;
    Image _Image;

    void Start()
    {
        _Image = GetComponent<Image>();
        Core.SubscribeEvent(Constants.OnPlayerHit, OnHit);
    }
    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.OnPlayerHit, OnHit);
    }
    void OnHit(object sender, object arg)
    {
        _CurrentTransitionTime = TransitionTime;
    }

    // Update is called once per frame
    void Update()
    {
        //blending to transparent
        Color color = _Image.color;
        color.a = (_CurrentTransitionTime / TransitionTime) / 4;
        _Image.color = color;

        if (_CurrentTransitionTime > 0)
        {
            _CurrentTransitionTime -= Time.deltaTime;
            _CurrentTransitionTime = Mathf.Max(0, _CurrentTransitionTime); //No less than zelo
        }
    }
}
