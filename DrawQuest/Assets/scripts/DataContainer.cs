﻿public static class DataContainer
{
    public static Skill[] Skills;
    public static PlayerInventory[] Invent;
    public static Player Player;
    public static WaveData[] WaveData;

    public static void GetSkillData()
    {
        Skills = DataManager.Instance.GetAllSkillsData();
    }

    public static void GetInventory()
    {
        Invent = DataManager.Instance.GetAllPlayerInventoryData();
    }

    public static void GetWaveData()
    {
        WaveData = DataManager.Instance.GetAllWaveData();
    }

    public static void GetPlayerData()
    {
        Player = DataManager.Instance.GetPlayerData();
    }

    /// <summary>
    /// Set skill in inventory
    /// </summary>
    /// <param name="inventid">which slot</param>
    /// <param name="skillid">id of the skill</param>
    public static void SetInventory(int inventid, int skillid)
    {
        PlayerInventory inv = new PlayerInventory();
        inv.inventory_id = inventid;
        inv.skill_id = skillid;
        DataManager.Instance.EditPlayerInventoryData(inv);
    }

    /// <summary>
    /// Set the highest wave the player has achieved
    /// </summary>
    /// <param name="highestwave">highest wave achieved</param>
    public static void SetPlayerHighestWave(int highestwave)
    {
        Player player = new Player();
        player.highest_wave = highestwave;
        player.checkpoint = Player.checkpoint;
        DataManager.Instance.EditPlayerData(player);
    }

    /// <summary>
    /// Set player checkpoint, every stage, every 10th wave after 1
    /// </summary>
    /// <param name="checkpoint">wave_id</param>
    public static void SetPlayerCheckpoint(int checkpoint)
    {
        Player player = new Player();
        player.checkpoint = checkpoint;
        player.highest_wave = Player.highest_wave;
        DataManager.Instance.EditPlayerData(player);
    }

    /// <summary>
    /// Set a skill as unlocked in database
    /// </summary>
    /// <param name="skillid">skill id</param>
    /// <param name="unlocked">0 is locked, 1 is unlocked</param>
    public static void SetUnlockedSkill(int skillid, int unlocked)
    {
        Skill skill = new Skill();
        skill.skill_id = skillid;
        skill.unlocked = unlocked;
        DataManager.Instance.EditSkillData(skill);
    }
}
