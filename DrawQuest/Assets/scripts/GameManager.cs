﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PDollarGestureRecognizer;
using System;

public class GameManager : MonoBehaviour
{
    #region Singleton
    static GameManager _instance = null;
    public static GameManager Instance { get { return _instance; } }
    void Awake()
    {
        if (_instance == null)
            _instance = this;
    }
    #endregion

    #region RuneStuff
    [SerializeField]
    GameObject _rune;
    [SerializeField]
    GameObject _runeSlots;
    List<GameObject> _runeList = new List<GameObject>();
    Result _result;
    List<string> _resultNames = new List<string>();
    int _correctCount = 0;

    [SerializeField]
    GameObject[] _slots;
    List<Skill> _skills = new List<Skill>();
    Dictionary<Skill, int> _skillCD = new Dictionary<Skill, int>();
    Skill _currentSkill;
    #endregion

    GameState _state = GameState.None;
    public GameState GameState { get { return _state; } }
    InternalGameState _currentInternalState = InternalGameState.Start;

    #region GameStuff
    [Header("Kyuu stuff")]
    PlayerDataHolder _kyuu = null;
    [SerializeField] GameObject _kyuuPrefab;
    [SerializeField] Vector3 _kyuuSpawnPos;
    [Header("Enemy stuff")]
    [SerializeField] GameObject _enemyPrefab;
    [SerializeField] Vector3 _enemySpawnPos;
    /// <summary>
    /// true if attacking phase, false if defending phase
    /// </summary>
    bool _attack = false;
    float _baseTime = 5f; //TODO : Change to be derived from database
    float _timer = 0;
    bool _startTimer = false;
    Enemy _currentEnemy = null;
    WaveData _currentWaveData = null;
    int _currentWave;
    float _waveDelay;
    [SerializeField] Text _currentWaveText;
    [SerializeField] Sprite[] _adsprites;
    [SerializeField] Image _adImage;
    [SerializeField] Image _timerImage;
    [SerializeField] Image _playerHealth;
    [SerializeField] Image _enemyHealth;
    Color[] _timerColors = { new Color(1f, 0, 0.2f, 1f), new Color(0, 0.8f, 1f, 1f) };
    [SerializeField]
    GameObject _endPanel;
    [SerializeField] GameObject _phasePanel;
    [SerializeField] Canvas _worldSpaceCanvas;
    PhasePanel _pp = null;
    bool _startPhase = false;
    #endregion

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(_kyuuSpawnPos, 0.2f);
        Gizmos.color = Color.green;
    }

    void Start()
    {
        //DataManager.Instance.Connect();
        EnemyManager.Instance.Init();
        Core.SubscribeEvent(Constants.OnGameStateChange, OnGameStateChange);
        Core.SubscribeEvent(Constants.OnRuneStartDraw, OnRuneStartDraw);
        Core.SubscribeEvent(Constants.Lose, SpawnEnd);
        LoadEquip();
    }

    void LoadEquip()
    {
        DataContainer.GetInventory();
        Sprite[] _sprites = Resources.LoadAll<Sprite>("Sprites/skills");
        for (int i = 0; i < _slots.Length; i++)
        {
            for (int j = 0; j < DataContainer.Invent.Length; j++)
            {
                if (DataContainer.Invent[j].skill_id == 0)
                    _slots[i].GetComponent<Image>().color = Vector4.zero;
                else if (DataContainer.Invent[j].inventory_id - 1 == i)
                {
                    _slots[i].GetComponent<Image>().sprite = _sprites[DataContainer.Invent[j].skill_id - 1];
                    _skills.Add(DataContainer.Skills[DataContainer.Invent[j].skill_id - 1]);
                }
            }
        }
    }

    void OnRuneStartDraw(object sender, object[] args)
    {
        if (!_startTimer)
        {
            _timer = _baseTime; //TODO : change to derived time based on wave
            _startTimer = true;
        }
    }

    /// <summary>
    /// On public game state change callback
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    void OnGameStateChange(object sender, object[] args)
    {
        GameState state = (GameState)args[0];
        switch (state)
        {
            case GameState.None:
                break;
        }
    }

    /// <summary>
    /// Check if drawn rune matches 
    /// </summary>
    void CheckDrawings()
    {
        for (int i = 0; i < _resultNames.Count; i++)
        {
            if (_result.GestureClass.Equals(_resultNames[i]))
            {
                //print("result : " + _resultNames[i]);
                _runeList[i].GetComponent<Image>().color = new Color(0.2f, 0.2f, 0.2f, 1);
                _runeList.RemoveAt(i);
                _resultNames.RemoveAt(i);
                _correctCount++;
                if(_correctCount == 5)
                {
                    _timer = 0; //Finish timer manually if all runes complete
                }
                break;
            }
        }

        for (int j = 0; j < _skills.Count; j++)
        {
            if(_result.GestureClass.Equals(_skills[j].technical_name))
            {
                if (_skillCD.ContainsKey(_skills[j]))
                {
                    //Debug.Log("skill on cooldown");
                    return;
                }
                else
                {
                    _skillCD.Add(_skills[j], _skills[j].cooldown);
                    _currentSkill = _skills[j];
                    if(_currentSkill.skill_id == 3)
                    {
                        _kyuu.HealMe();
                    }
                    else
                    {
                        ChangeInternalState(InternalGameState.Skill);
                        _timer = 0;
                    }
                }
            }
        }
    }

    void GenerateRunes()
    {
        for (int i = 0; i < 5; i++)
        {
            GameObject o = Instantiate(_rune, _runeSlots.transform, false);
            _runeList.Add(o);
            string s = o.GetComponent<Rune>().GetRuneType();
            _resultNames.Add(s);
        }
    }

    void ClearRunes()
    {
        if(_runeSlots.transform.childCount > 0)
        {
            for (int i = 0; i < 5; i++)
            {
                Destroy(_runeSlots.transform.GetChild(i).gameObject);
            }
        }
        _runeList.Clear();
        _resultNames.Clear();
        _correctCount = 0;
    }

    public void AddResult(Result res)
    {
        _result = res;
        CheckDrawings();
    }

    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.OnGameStateChange, OnGameStateChange);
        Core.UnsubscribeEvent(Constants.OnRuneStartDraw, OnRuneStartDraw);
        Core.UnsubscribeEvent(Constants.Lose, SpawnEnd);
        if (_instance != null)
            _instance = null;
    }

    void SpawnEnd(object sender, object[] args)
    {
        GameObject o = Instantiate(_endPanel, GameObject.FindObjectOfType<Canvas>().transform, false);
        o.GetComponent<End>().SetWaves(_currentWaveData.wave_id.ToString());
    }

    void Update()
    {
        GameLoop();
    }

    void GameLoop()
    {
        switch (_currentInternalState)
        {
            case InternalGameState.Start:
                //Spawn Kyuu  
                if (_kyuu == null)
                {
                    _kyuu = Instantiate(_kyuuPrefab, _kyuuSpawnPos, Quaternion.identity).GetComponent<PlayerDataHolder>();
                    _kyuu.Init();
                }
                //GetWaveData
                if(_currentWaveData == null)
                {
                    int checkpoint = 10 * (DataContainer.Player.checkpoint - 1);
                    _currentWaveData = DataContainer.WaveData[checkpoint];
                    _currentWave = _currentWaveData.wave_id;
                }
                else
                {
                    _currentWaveData = null;
                    _currentWave++;
                    _currentWaveData = DataContainer.WaveData[_currentWave - 1];
                }
                _currentWaveText.text = "Wave " + _currentWaveData.wave_id;
                //Spawn Enemy 
                EnemyData ed = EnemyManager.Instance.GetEnemyData(_currentWaveData.enemy_type);
                _currentEnemy = Instantiate(_enemyPrefab, _enemySpawnPos, Quaternion.identity).GetComponent<Enemy>();
                _currentEnemy.Init(ed, _currentWaveData.stage);
                //Show whatever effects thingy 
                //Initialize whatever 
                _enemyHealth.fillAmount = 1f;
                _timerImage.fillAmount = 1f;
                _attack = true;
                ChangeInternalState(InternalGameState.Generating);
                ChangePublicState(GameState.GameStopDraw);
                break;
            case InternalGameState.Generating:
                ClearRunes();
                GenerateRunes();
                DeductCD();
                if (_attack)
                {
                    ChangeInternalState(InternalGameState.Attack);
                }
                else
                {
                    ChangeInternalState(InternalGameState.Defend);
                }
                break;
            case InternalGameState.Skill:
                //Debug.Log("cast skill");
                _startTimer = false;
                if (CalculateSkill())
                {
                    ChangeInternalState(InternalGameState.Move);
                }
                else
                {
                    ChangeInternalState(InternalGameState.PlaySkillAnimation);
                    _waveDelay = 2f;
                }
                ChangePublicState(GameState.GameStopDraw);
                break;
            case InternalGameState.Attack:
                if (!_startPhase)
                {
                    if(_pp == null)
                    {
                        _pp = Instantiate(_phasePanel, _worldSpaceCanvas.transform).GetComponent<PhasePanel>();
                        _pp.Init(0, _timerColors[0]);
                        //_startPhase = true;
                    }
                    _timerImage.fillAmount = 1f;
                    _timerImage.color = _timerColors[0];
                    _adImage.sprite = _adsprites[0];
                    _adImage.color = _timerColors[0];
                    if (_pp.Active)
                        return;

                    Destroy(_pp.gameObject);
                    _pp = null;
                    ChangePublicState(GameState.GameCanDraw);
                    _startPhase = true;
                }
                if (!_startTimer)
                    return;
                //Wait for startdraw to start loop
                //Start timer when drawing runes
                if (_startTimer && _timer > 0)
                {
                    _timer -= Time.deltaTime;
                    _timerImage.fillAmount = _timer / _baseTime;
                    return;
                }
                _startTimer = false;
                //Finish runes
                if (CalculateDamageAttack())
                {
                    ChangeInternalState(InternalGameState.Move);
                }
                else
                {
                    ChangeInternalState(InternalGameState.PlayAnimation);
                    _waveDelay = 2f;
                }
                _startPhase = false;
                ChangePublicState(GameState.GameStopDraw);
                break;
            case InternalGameState.Defend:
                if (!_startPhase)
                {
                    if (_pp == null)
                    {
                        _pp = Instantiate(_phasePanel, _worldSpaceCanvas.transform).GetComponent<PhasePanel>();
                        _pp.Init(1, _timerColors[1]);
                    }
                    _timerImage.fillAmount = 1f;
                    _timerImage.color = _timerColors[1];
                    _adImage.sprite = _adsprites[1];
                    _adImage.color = _timerColors[1];
                    if (_pp.Active)
                        return;

                    Destroy(_pp.gameObject);
                    _pp = null;
                    _startPhase = true;
                }
                ChangePublicState(GameState.GameCanDraw);

                if (!_startTimer)
                    return;
                //Wait for startdraw to start loop
                //Start timer when drawing runes
                if (_startTimer && _timer > 0)
                {
                    _timer -= Time.deltaTime;
                    _timerImage.fillAmount = _timer / _baseTime;
                    return;
                }
                //Finish runes
                _startTimer = false;
                if (CalculateDamageDefend())
                {
                    ChangeInternalState(InternalGameState.PlayerDead);
                }
                else
                {
                    ChangeInternalState(InternalGameState.PlayAnimation);
                    _waveDelay = 2f;
                }
                _startPhase = false;
                ChangePublicState(GameState.GameStopDraw);
                break;
            case InternalGameState.PlayAnimation:
                if (_attack)
                {
                    if (_correctCount > 0)
                    {
                        //play animation 
                        _kyuu.SetAttackAnim();
                        if (_kyuu.PlayingAnimation)
                            return;
                    }

                    _kyuu.Attacked = false;
                    //check PlayingAnimation
                    //return;
                    //only change state after animations complete
                    _enemyHealth.fillAmount = (float)_currentEnemy.CurrentHealth / (float)_currentEnemy.MaxHealth;
                    _attack = false;
                }
                else
                {
                    if(_correctCount != 5)
                    {
                        Core.BroadcastEvent(Constants.OnPlayerHit, this);
                        _kyuu.SetHurtAnim();
                    }
                    //play enemy attack animations
                    //only change state after animations complete
                    _playerHealth.fillAmount = (float)_kyuu.CurrentHealth/ (float)_kyuu.MaxHealth;
                    _attack = true;
                }
                _kyuu.ResetAnim();
                _startPhase = false;
                ChangeInternalState(InternalGameState.Generating);
                break;
            case InternalGameState.PlaySkillAnimation:            
                if (_currentSkill != null)
                {                   
                    if (_currentSkill.skill_id == 1)
                    {
                        _kyuu.SetSkillAnim();
                        if (_kyuu.PlayingAnimation)
                            return;
                    }
                    else if(_currentSkill.skill_id == 2)
                    {
                        _kyuu.InstLifeSteal();
                        if (_kyuu.PlayingAnimation)
                            return;
                    }
                    if (_currentSkill.skill_id == 3)
                    {
                        _kyuu.InstInvulnerability();
                    }
                    else if(_currentSkill.skill_id == 4)
                    {
                        _kyuu.InstHeal();
                    }
                    _currentSkill = null;
                }
                //play healing anim
                _playerHealth.fillAmount = (float)_kyuu.CurrentHealth / (float)_kyuu.MaxHealth;
                _enemyHealth.fillAmount = (float)_currentEnemy.CurrentHealth / (float)_currentEnemy.MaxHealth;
                _attack = !_attack;
                _kyuu.ResetAnim();
                _startPhase = false;
                ChangeInternalState(InternalGameState.Generating);          
                break;
            case InternalGameState.Move:
                if (_currentSkill != null)
                {
                    _kyuu.SetSkillAnim();
                    if (_kyuu.PlayingAnimation)
                        return;
                }
                else
                {
                    //play animation 
                    _kyuu.SetAttackAnim();
                    if (_kyuu.PlayingAnimation)
                        return;
                }

                _enemyHealth.fillAmount = (float)_currentEnemy.CurrentHealth / (float)_currentEnemy.MaxHealth;
                _currentEnemy.SetDeathAnim();
                if(_currentEnemy.PlayingAnimation)
                    return;
                
                _currentSkill = null;
                _kyuu.Attacked = false;
                SaveCheckpoint(_currentWave);
                SaveHighestWave(_currentWave);
                if (_currentEnemy != null)
                {
                    Destroy(_currentEnemy.gameObject);
                    _currentEnemy = null;
                }
                _kyuu.ResetAnim();
                _startPhase = false;
                ChangeInternalState(InternalGameState.Start);
                //play victory anims and movement anims/new enemy spawning animations
                //ChangeInternalState(InternalGameState.Start);
                break;
            case InternalGameState.PlayerDead:
                _kyuu.SetDeathAnim();
                //play dead animations and end the game 
                //ChangeInternalState(InternalGameState.Start);
                break;
        }
    }

    /// <summary>
    /// If true, player is dead and change to player dead state
    /// </summary>
    /// <returns></returns>
    bool CalculateDamageDefend()
    {
        int enemyattack = _currentEnemy.GetAttackValue();
        int damagetaken = _resultNames.Count * enemyattack;
        return _kyuu.CheckDead(damagetaken);
    }

    /// <summary>
    /// If true, enemy is dead and change to move state
    /// </summary>
    /// <returns></returns>
    bool CalculateDamageAttack()
    {
        int multiplier = _correctCount;
        //Do calculation logic here
        return _currentEnemy.CheckDead(_correctCount * _kyuu.GetAttackValue());
    }

    bool CalculateSkill()
    {
        switch(_currentSkill.type)
        {
            case 0:
                return _currentEnemy.CheckDead(_currentSkill.damage);
            case 1:
                int enemyattack = _currentEnemy.GetAttackValue();
                int damagetaken = enemyattack - _currentSkill.damage;
                if(_currentSkill.skill_id == 3)
                    return _kyuu.CheckDead(0);
                else
                    return _kyuu.CheckDead(damagetaken);
        }
        return false;
    }

    void DeductCD()
    {
        List<Skill> temp = new List<Skill>(_skillCD.Keys);
        foreach (Skill skill in temp)
        {
            //Debug.Log(skill.name);
            int val = 0;
            if(_skillCD.TryGetValue(skill, out val))
            {
                if(val != -1)
                {
                    val -= 1;
                    _skillCD[skill] = val;
                }
                else if(val == -1)
                {
                    _skillCD.Remove(skill);
                }
            }
            val += 1;
            if(_skills.Contains(skill))
            {
                int x = _skills.IndexOf(skill);
                GameObject o = _slots[x].transform.GetChild(0).gameObject;
                if(val != 0)
                {
                    o.SetActive(true);
                    o.GetComponentInChildren<Text>().text = val.ToString();
                }
                else if (val == 0)
                {
                    o.SetActive(false);
                }
            }
        }
    }

    void ChangeInternalState(InternalGameState state)
    {
        if (_currentInternalState == state)
            return;

        _currentInternalState = state;
    }

    void ChangePublicState(GameState state)
    {
        if (_state == state)
            return;

        _state = state;
        Core.BroadcastEvent(Constants.OnGameStateChange, this, _state);
    }

    void SaveCheckpoint(int currentWave)
    {
        if(currentWave%10 == 0)
        {
            DataContainer.SetPlayerCheckpoint(currentWave / 10);
        }
    }

    void SaveHighestWave(int currentWave)
    {
        DataContainer.SetPlayerHighestWave(currentWave);
    }
}

/// <summary>
/// public game state 
/// </summary>
public enum GameState
{
    None,
    GameCanDraw,
    GameStopDraw,
    End
}

/// <summary>
/// Internal Game State is only for game manager to change
/// </summary>
public enum InternalGameState
{
    /// <summary>
    /// For the start of the wave, should only run once
    /// </summary>
    Start,
    /// <summary>
    /// For generating runes, must always run once before attacking or defending
    /// </summary>
    Generating,
    /// <summary>
    /// For casting skills
    /// </summary>
    Skill,
    /// <summary>
    /// For attacking state
    /// </summary>
    Attack,
    /// <summary>
    /// For defending state
    /// </summary>
    Defend,
    /// <summary>
    /// When finish attacking/defending to play animations
    /// </summary>
    PlayAnimation,
    /// <summary>
    /// When finish skill to play animations
    /// </summary>
    PlaySkillAnimation,
    /// <summary>
    /// When wave cleared and moving to next year
    /// </summary>
    Move,
    /// <summary>
    /// If player gets killed by enemy
    /// </summary>
    PlayerDead
}
