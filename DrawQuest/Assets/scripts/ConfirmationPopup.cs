﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmationPopup : MonoBehaviour
{
    [SerializeField]
    Button _yesBtn;
    [SerializeField]
    Button _noBtn;
    
	void Start ()
    {
        _yesBtn.onClick.AddListener(YesClicked);
        _noBtn.onClick.AddListener(NoClicked);
    }

    void YesClicked()
    {
        ClosePanel(1);
    }

    void NoClicked()
    {
        ClosePanel(0);
    }

    void ClosePanel(int x)
    {
        Destroy(this.gameObject);
        UIManager.Instance.CloseDialog(x);
    }
}
