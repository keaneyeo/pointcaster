﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuneDescription : MonoBehaviour
{
    [SerializeField]
    Image _img;
    [SerializeField]
    Text _name;
    [SerializeField]
    Text _description;
    [SerializeField]
    Button _equipBtn;
    [SerializeField]
    Button _cancelBtn;
    [SerializeField]
    GameObject _slotPanel;

    Skill _skill;

	void Start ()
    {
        _equipBtn.onClick.AddListener(EquipSkill);
        _cancelBtn.onClick.AddListener(Close);
	}

    public void SetStats(Skill skill)
    {
        _skill = skill;
        Sprite[] _sprites = Resources.LoadAll<Sprite>("Sprites/skills");
        _img.sprite = _sprites[skill.skill_id - 1];
        _name.text = skill.name;
        _description.text = skill.description;
    }
	
	void OnDestroy ()
    {
        _equipBtn.onClick.RemoveAllListeners();
        _cancelBtn.onClick.RemoveAllListeners();
	}

    void EquipSkill()
    {
        Instantiate(_slotPanel, this.transform, false);
    }

    public void Equip(int x)
    {
        DataContainer.SetInventory(x, _skill.skill_id);
        Core.BroadcastEvent(Constants.UpdateEquip, this);
        Close();
    }

    void Close()
    {
        Destroy(this.gameObject);
    }
}
