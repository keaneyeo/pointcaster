﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public static class ConfigParser
{
    public static Dictionary<string, string> SettingsDictionary = new Dictionary<string, string>();
    static string _filePath;
    static string _default = Application.persistentDataPath + "/" + "settings.config";

    public static void LoadConfig(string path)
    {
        _filePath = path;
        if(File.Exists(path))
        {
            SetDictionary();
        }
        else
        {
            Debug.Log("'settings.config' not detected. Generating new config file.");
            GenerateConfig();
        }
    }
    
    public static bool ParseBool(string s)
    {
        bool boolean = true;
        if (bool.TryParse(s, out boolean))
            return boolean;
        else
            return true;
    }

    public static float ParseFloat(string s)
    {
        float value = 0;
        if (float.TryParse(s, out value))
            return value;
        else
            return 1;
    }

    public static void WriteConfig()
    {
        if (_filePath.Equals(""))
            _filePath = _default;
        using (StreamWriter sw = new StreamWriter(_filePath))
        {
            foreach (string settingtype in SettingsDictionary.Keys)
            {
                string s = "";
                SettingsDictionary.TryGetValue(settingtype, out s);
                sw.WriteLine(settingtype + "=" + s);
            }
        }
    }

    static void GenerateConfig()
    {
        string[] a = new string[]
        {
            //"master=true",
            "bgm=true",
            "sfx=true",
            //"mastervol=10.0",
            "bgmvol=10.0",
            "sfxvol=10.0"
        };
        using (StreamWriter sw = new StreamWriter(_filePath))
        {
            foreach (string s in a)
            {
                sw.WriteLine(s);
            }
        }
        SetDictionary();
    }

    static void SetDictionary()
    {
        string[] _settings = File.ReadAllLines(_filePath);
        foreach (string configType in _settings)
        {
            int x = configType.IndexOf("=") + 1;
            string a = configType.Substring(0, x - 1);
            string b = configType.Substring(x, configType.Length - x);
            SettingsDictionary.Add(a, b.ToLower());
        }
    }
}
