﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<T> : MonoBehaviour where T : Component
{
    public static ObjectPool<T> Instance { get; private set; }

    [SerializeField] protected int _PooledAmount = 5;
    [SerializeField] protected bool _CanGrow = true;

    protected List<T> _Pool;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void Start()
    {
        _Pool = new List<T>();
        for (int i = 0; i < _PooledAmount; i++)
        {
            CreatePooledObject();
        }
    }

    public T GetPooledObject()
    {
        for (int i = 0; i < _Pool.Count; i++)
        {
            if (!_Pool[i].gameObject.activeInHierarchy)
            {
                return _Pool[i];
            }
        }
        if (_CanGrow)
        {
            return CreatePooledObject();
        }
        return null;
    }

    protected virtual T CreatePooledObject()
    {
        GameObject obj = new GameObject("PoolComponent");
        obj.transform.SetParent(transform);
        T poolType = obj.AddComponent<T>();
        obj.SetActive(false);
        _Pool.Add(poolType);
        return poolType;
    }
}
