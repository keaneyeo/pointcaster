﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioComponentPool : ObjectPool<AudioSource>
{
    protected AudioComponentPool() { }

    private void LateUpdate()
    {
        foreach (var obj in _Pool)
        {
            if (!obj.isPlaying)
            {
                obj.Stop();
                obj.gameObject.SetActive(false);
            }
        }
    }
}
