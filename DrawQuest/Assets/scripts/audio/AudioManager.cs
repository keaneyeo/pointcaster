﻿using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    static AudioManager _instance = null;
    public static AudioManager Instance { get { return _instance; } }

    [SerializeField]
    AudioMixer _masterMixer;
    [SerializeField]
    AudioMixerGroup _BGMGroup;
    [SerializeField]
    AudioMixerGroup _SFXGroup;

    [Header("BGM Clips")]
    [SerializeField] AudioClip[] _BGMClips;
    [Header("SFX Clips")]
    [SerializeField] AudioClip[] _SFXClips;

    AudioSource _BGMSource = null;

	void Awake ()
    {
        if (_instance == null)
            _instance = this;

        if (_BGMSource == null)
            _BGMSource = gameObject.AddComponent<AudioSource>();
        Debug.Log("bgm: " + AudioSettings.BGM);
        Debug.Log("sfx: " + AudioSettings.SFX);
        Debug.Log("bgmvol: " + AudioSettings.BGMVol);
        Debug.Log("sfxvol: " + AudioSettings.SFXVol);
        UpdateMixer();
    }

    void Start()
    {
        _BGMSource.outputAudioMixerGroup = _BGMGroup;
        _BGMSource.clip = _BGMClips[0];
        _BGMSource.loop = true;
        _BGMSource.playOnAwake = true;
        _BGMSource.Play();
    }

    void OnDestroy()
    {
        if (_instance != null)
            _instance = null;
    }
    
    public void PlaySound(SoundType type)
    {
        AudioSource audioSource = AudioComponentPool.Instance.GetPooledObject();
        if (audioSource == null)
            return;

        audioSource.gameObject.SetActive(true);
        audioSource.outputAudioMixerGroup = _SFXGroup;

        AudioClip clip = null;
        clip = _SFXClips[(int)type];
        audioSource.PlayOneShot(clip);
    }

    public void SetMusic(LevelType levelType)
    {
        if (_BGMSource.isPlaying)
            _BGMSource.Stop();
        _BGMSource.clip = _BGMClips[(int)levelType];
        _BGMSource.Play();
    }

    public void UpdateMixer()
    {
        float max = 10;
        float min = 1;
        float range = max - min; 
        float nmax = 0;
        float nmin = -20;
        float nrange = nmax - nmin;

        if(AudioSettings.BGM == false)
            _masterMixer.SetFloat("BGMVol", -80);
        else
            _masterMixer.SetFloat("BGMVol", (((AudioSettings.BGMVol - min)*nrange)/range)+nmin);

        if (AudioSettings.SFX == false)
            _masterMixer.SetFloat("SFXVol", -80);
        else
            _masterMixer.SetFloat("SFXVol", (((AudioSettings.SFXVol - min) * nrange) / range) + nmin);
    }
}

public enum SoundType
{
    btn,
    attack,
    heal,
    shield,
    lightning
}

public enum LevelType
{
    Main,
    Game
}

public static class AudioSettings
{
    //public static bool Master = false;
    public static bool BGM = false;
    public static bool SFX = false;
    //public static float MasterVol = 0f;
    public static float BGMVol = 0f;
    public static float SFXVol = 0f;
}