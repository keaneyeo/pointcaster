﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using PDollarGestureRecognizer;

public class GestureChecker : MonoBehaviour
{
    List<Gesture> _gestureList = new List<Gesture>();
    List<Point> _pointList = new List<Point>();
    //List<LineRenderer> _lineRendererList = new List<LineRenderer>(); //multiline gestures
    [SerializeField]
    Transform _visualRenderer;
    LineRenderer _lineRenderer;
    Rect _drawingArea;
    Vector3 _pointPos = Vector2.zero;
    bool _recognized = false;
    private int _strokeID = -1;
    private int _vertexCount = 0;
    bool _startedDraw = false;

    private void Start()
    {
        _drawingArea = new Rect(0, 0, Screen.width, Screen.height);
        //Debug.Log(drawingArea.position + " " + drawingArea.center);
        
        TextAsset[] gesturesXml = Resources.LoadAll<TextAsset>("Gestures/");
        foreach (TextAsset gestureXml in gesturesXml)
        {
            _gestureList.Add(GestureIO.ReadGestureFromXML(gestureXml.text));
        }

        //        string[] filePaths = Directory.GetFiles(Application.persistentDataPath, "*.xml");

        //        foreach (string filePath in filePaths)
        //        {
        //            _gestureList.Add(GestureIO.ReadGestureFromFile(filePath));
        //        }

        //        string assetpaths = Application.dataPath + "/StreamingAssets";
        //#if UNITY_ANDROID && !UNITY_EDITOR
        //        assetpaths = "jar:file://" + Application.dataPath + "!/assets";
        //#elif UNITY_IOS
        //        assetpaths = Application.dataPath + "/Raw";
        //#else

        //#endif
        //        string[] filePaths0 = Directory.GetFiles(assetpaths, "*.xml");
        //        Debug.Log(filePaths.Length +" files to parse");

        //        //foreach(string filePaths)
        //#if UNITY_ANDROID || UNITY_IOS
        //#endif
        //        foreach (string filepath_ in filePaths0)
        //        {
        //            _gestureList.Add(GestureIO.ReadGestureFromFile(filepath_));
        //        }

        Core.SubscribeEvent(Constants.OnGameStateChange, ClearDrawing);
    }

    private void Update()
    {
#if UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount > 0 && GameManager.Instance.GameState == GameState.GameCanDraw)
        {
            _pointPos = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
            if (!_startedDraw)
            {
                Core.BroadcastEvent(Constants.OnRuneStartDraw, this);
                _startedDraw = true;
            }
        }
#else
        if (Input.GetMouseButton(0) && GameManager.Instance.GameState == GameState.GameCanDraw)
        {
            _pointPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y);
            if (!_startedDraw)
            {
                Core.BroadcastEvent(Constants.OnRuneStartDraw, this);
                _startedDraw = true;
            }
        }
#endif
        if (_drawingArea.Contains(_pointPos) && GameManager.Instance.GameState == GameState.GameCanDraw)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ClearStroke();
                
                ++_strokeID;

                Transform tmpGesture = Instantiate(_visualRenderer, transform.position, transform.rotation) as Transform;
                _lineRenderer = tmpGesture.GetComponent<LineRenderer>();

                //_lineRendererList.Add(_lineRenderer);

                _vertexCount = 0;
            }
            
            if (Input.GetMouseButton(0))
            {
                _pointList.Add(new Point(_pointPos.x, -_pointPos.y, _strokeID));

                _lineRenderer.positionCount = ++_vertexCount;
                _lineRenderer.SetPosition(_vertexCount - 1, Camera.main.ScreenToWorldPoint(new Vector3(_pointPos.x, _pointPos.y, 10)));
            }

            //when release screen
            if (Input.GetMouseButtonUp(0))
            {
                _recognized = true;

                Gesture candidate = new Gesture(_pointList.ToArray());
                Result gestureResult = PointCloudRecognizer.Classify(candidate, _gestureList.ToArray());
                GameManager.Instance.AddResult(gestureResult);
                _startedDraw = false;
                //Debug.Log(gestureResult.GestureClass + " " + gestureResult.Score);
                
                if (_recognized)
                {
                    ClearStroke();
                }
            }
        }
    }

    void ClearDrawing(object sender, object[] args)
    {
        GameState gs = (GameState)args[0];
        if (gs == GameState.GameStopDraw && _lineRenderer != null)
            ClearStroke();
    }

    public void ClearStroke()
    {
        _recognized = false;
        _strokeID = -1;
        _pointList.Clear();

        if(_lineRenderer != null)
            Destroy(_lineRenderer.gameObject);

        //foreach (LineRenderer lineRenderer in _lineRendererList)
        //{
        //    lineRenderer.positionCount = 0;
        //    Destroy(lineRenderer.gameObject);
        //}
        //_lineRendererList.Clear();
    }
}
