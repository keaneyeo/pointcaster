﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameHud : MonoBehaviour
{
    [SerializeField]
    Button _quitBtn;

    void Start()
    {
        _quitBtn.onClick.AddListener(()=> { UIManager.Instance.OpenDialog(); });
	}

    void OnDestroy()
    {
        _quitBtn.onClick.RemoveAllListeners();
    }
}
