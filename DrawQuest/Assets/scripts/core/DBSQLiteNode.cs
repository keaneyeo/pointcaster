﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using Mono.Data.Sqlite;
using System;

public class DBSQLiteNode : DBNode
{
    public override DBMSType Type { get { return DBMSType.SQLite; } }

    public override int ExectueNonQuery(string sql, KeyValuePair<string, object>[] args = null)
    {
        try
        {
            using (IDbCommand command = _dbConnection.CreateCommand())
            {
                command.CommandText = sql;
                if (args != null)
                {
                    foreach (var v in args)
                    {
                        IDbDataParameter p = command.CreateParameter();
                        p.ParameterName = v.Key;
                        p.Value = v.Value;
                        command.Parameters.Add(p);
                    }
                }
                return command.ExecuteNonQuery();
            }
        }
        catch(SqliteException e)
        {
            Debug.Log(e.Message);
        }
        catch(Exception e)
        {
            Debug.Log(e.Message);
        }
        return 0;
    }

    public override IDataReader ExecuteWithQuery(string sql, KeyValuePair<string, object>[] args = null)
    {
        IDataReader reader = null;
        try
        {
            using (IDbCommand command = _dbConnection.CreateCommand())
            {
                command.CommandText = sql;
                if(args != null)
                {
                    foreach(var v in args)
                    {
                        IDbDataParameter p = command.CreateParameter();
                        p.ParameterName = v.Key;
                        p.Value = v.Value;
                        command.Parameters.Add(p);
                    }
                }
                reader = command.ExecuteReader();
            }
        }
        catch (SqliteException e)
        {
            Debug.Log(e.Message);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
        return reader;
    }

    protected override IDbConnection _CreateDBConnection(string connectionstring)
    {
        return new SqliteConnection(connectionstring);
    }
}
