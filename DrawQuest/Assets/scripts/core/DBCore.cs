﻿using System.Data;
using System;
using System.Collections.Generic;

public abstract class DBNode
{
    protected IDbConnection _dbConnection = null;
    protected abstract IDbConnection _CreateDBConnection(string connectionstring);
    public abstract DBMSType Type { get; }
    public ConnectionState State
    {
        get
        {
            ConnectionState state = ConnectionState.Closed;
            if (_dbConnection != null)
                state = _dbConnection.State;
            return state;
        }
    }
    public void ConnectToDB(string connectionstring)
    {
        if(_dbConnection == null)
        {
            _dbConnection = _CreateDBConnection(connectionstring);
            _dbConnection.Open();
        }
        else if(_dbConnection.State == ConnectionState.Closed)
        {
            _dbConnection.Dispose();
            _dbConnection = null;
        }
    }
    public abstract int ExectueNonQuery(string sql, KeyValuePair<string, object>[] args = null);
    public abstract IDataReader ExecuteWithQuery(string sql, KeyValuePair<string, object>[] args = null);

    public void DisconnectDB()
    {
        _dbConnection.Close();
        _dbConnection.Dispose();
        _dbConnection = null;
    }

    public bool ExecuteTransaction<T>(ExecuteCommand command, 
                                        ref T[] result, Parser parser,
                                        params KeyValuePair<string, object>[] args)
    {
        using (IDbTransaction transaction = _dbConnection.BeginTransaction())
        {
            IDataReader reader = null;
            if (command(_dbConnection, ref reader, args))
                transaction.Commit();
            else
            {
                transaction.Rollback();
                return false;
            }
            List<T> _listresults = new List<T>();
            if(result != null)
                _listresults.AddRange(result);
            while (reader.Read())
            {
                object r = parser(reader);
                if (r != null)
                    _listresults.Add((T)r);
            }
            result = _listresults.ToArray();
            return true;
        }
    }
}

public enum DBMSType
{
    SQLite,
    PostgreSQL //???
}

public delegate bool ExecuteCommand(IDbConnection connection, 
                                        ref IDataReader reader,
                                        params KeyValuePair<string, object>[] args);
public delegate object Parser(IDataReader reader);

public class Parameterizer
{
    public List<KeyValuePair<string, object>> Bag = new List<KeyValuePair<string, object>>();
    public void AddParameter(string key, object value)
    {
        Bag.Add(new KeyValuePair<string, object>(key, value));
    }
}