﻿using System.Collections;
using System.Collections.Generic;

public static partial class Core
{
    public delegate void GameEvent(object sender, object[] args);

    static Dictionary<string, GameEvent> _EventBag = new Dictionary<string,GameEvent>();

    public static void SubscribeEvent(string eventName, GameEvent gameEvent)
    {
        GameEvent currentEvent = null;
        if(!_EventBag.TryGetValue(eventName, out currentEvent))
        {
            currentEvent = gameEvent;
        }
        else
        {
            currentEvent += gameEvent;//delegate subscribe
        }

        _EventBag[eventName] = currentEvent;
    }
    public static void UnsubscribeEvent(string eventName, GameEvent gameEvent)
    {
        GameEvent currentEvent = null;
        if (_EventBag.TryGetValue(eventName, out currentEvent))
        {
            currentEvent -= gameEvent;

            if (currentEvent == null)
            {
                _EventBag.Remove(eventName);
            }
            else
            {
                _EventBag[eventName] = currentEvent;
            }
        }
    }
    public static void BroadcastEvent(string eventName, object sender, params object[] args)
    {
        GameEvent currentEvent = null;
        if(_EventBag.TryGetValue(eventName, out currentEvent))
        {
            currentEvent(sender, args);
        }
    }
    public static void ClearAll()
    {
        _EventBag.Clear();
    }
}
