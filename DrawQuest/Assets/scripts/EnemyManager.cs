﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    static EnemyManager _instance;
    public static EnemyManager Instance { get { return _instance; } }
    static List<EnemyData> _enemiesDataList = new List<EnemyData>();
    public Sprite[] NormalEnemySprites;
    public Sprite[] BossSprites;
    public AnimatorOverrideController[] NormalEnemyAnims;
    void Awake()
    {
        _instance = this;
    }

    void OnDestroy()
    {
        _instance = null;
    }

    public bool Init()
    {
        EnemyData[] enemydata = DataManager.Instance.GetAllEnemyData();
        for (int i = 0; i < enemydata.Length; i++)
        {
            _enemiesDataList.Add(enemydata[i]);
        }
        return true;
    }
	
	public EnemyData GetEnemyData(int enemytype)
    {
        return _enemiesDataList[enemytype];
    }
}
