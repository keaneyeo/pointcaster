﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotPanel : MonoBehaviour
{
    [SerializeField]
    Button _slotOne;
    [SerializeField]
    Button _slotTwo;
    [SerializeField]
    Button _slotThree;
    [SerializeField]
    Button _slotFour;
    [SerializeField]
    Button _cancelBtn;

    void Start()
    {
        _slotOne.onClick.AddListener(() => { GetComponentInParent<RuneDescription>().Equip(1); });
        _slotTwo.onClick.AddListener(() => { GetComponentInParent<RuneDescription>().Equip(2); });
        _slotThree.onClick.AddListener(() => { GetComponentInParent<RuneDescription>().Equip(3); });
        _slotFour.onClick.AddListener(() => { GetComponentInParent<RuneDescription>().Equip(4); });
        _cancelBtn.onClick.AddListener(() => { Destroy(this.gameObject); });
    }

    void OnDestroy()
    {
        _slotOne.onClick.RemoveAllListeners();
        _slotTwo.onClick.RemoveAllListeners();
        _slotThree.onClick.RemoveAllListeners();
        _slotFour.onClick.RemoveAllListeners();
        _cancelBtn.onClick.RemoveAllListeners();
    }
}
