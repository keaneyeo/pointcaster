﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataHolder : MonoBehaviour
{
    public GameObject Fireball;
    public GameObject Lightning;
    public GameObject Heal;
    public GameObject Invul;

    GameObject _invulShield = null;
    int _baseHealth;
    int _attack;
    int _currentHealth;
    public int CurrentHealth { get { return _currentHealth; } }
    public int MaxHealth { get { return _baseHealth; } }
    Animator _anim;
    public bool PlayingAnimation = false;
    public bool Attacked = false;
    public bool Init()
    {
        Player p = DataManager.Instance.GetPlayerData();
        _baseHealth = p.health;
        _attack = p.attack;
        _currentHealth = _baseHealth;
        _anim = GetComponent<Animator>();
        return true;
    }

    public int GetAttackValue()
    {
        return _attack;
    }

    /// <summary>
    /// Check if player dead, if true player dead
    /// </summary>
    /// <param name="damage">damage to be taken by player</param>
    /// <returns></returns>
    public bool CheckDead(int damage)
    {
        _currentHealth -= damage;
        Debug.Log("Kyuu health" + _currentHealth);
        if (_currentHealth <= 0)
            return true;
        else
            return false;
    }

    /// <summary>
    /// Set attack animation bool
    /// </summary>
    /// <param name="attack">true = play, false = return to idle</param>
    public void SetAttackAnim()
    {
        if (!Attacked)
        {
            _anim.SetTrigger("attack");
            PlayingAnimation = true;
            Attacked = true;
            InstFb();
        }
    }

    public void SetSkillAnim()
    {
        if (!Attacked)
        {
            _anim.SetTrigger("skill1");
            PlayingAnimation = true;
            Attacked = true;
        }
    }

    public void SetHurtAnim()
    {
        _anim.SetTrigger("hurt");
    }

    public void SetDeathAnim()
    {
        _anim.SetTrigger("death");
    }

    public void ResetAnim()
    {
        _anim.ResetTrigger("hurt");
        _anim.SetBool("attack", false);
        if (_invulShield != null)
            Destroy(_invulShield.gameObject);
    }

    public void HealMe()
    {
        _currentHealth = _baseHealth;
    }
    public void InstFb()
    {
        Instantiate(Fireball, transform.position, Quaternion.identity);
    }

    public void InstLightning()
    {
        Instantiate(Lightning, Lightning.transform.position, Quaternion.identity);
    }

    public void InstHeal()
    {
        Instantiate(Heal, Heal.transform.position, Quaternion.identity);
    }

    public void InstInvulnerability()
    {
        _invulShield = Instantiate(Invul, Invul.transform.position, Quaternion.identity);
    }

    public void InstLifeSteal()
    {
        if (!Attacked)
        {
            _anim.SetTrigger("attack");
            PlayingAnimation = true;
            Attacked = true;
            InstFb();
            InstHeal();
        }
    }
}
