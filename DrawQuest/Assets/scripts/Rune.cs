﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rune : MonoBehaviour
{
    public RunePattern _pattern = RunePattern.none;
    Image _img;

    void Awake()
    {
        Sprite[] _sprites = Resources.LoadAll<Sprite>("Sprites/runes");
        _img = GetComponent<Image>();
        int x = Random.Range(0, (int)RunePattern.wrong);
        _pattern = (RunePattern)x;
        _img.sprite = _sprites[x];
    }

    public string GetRuneType()
    {
        return _pattern.ToString();
    }
}

public enum RunePattern
{
    R1C1 = 0,
    R1C2,
    R1C3,
    R1C4,
    R2C1,
    R2C2,
    R2C3,
    R2C4,
    R3C1,
    R3C2,
    R3C3,
    R3C4,
    R4C1,
    R4C2,
    R4C3,
    R4C4,
    R5C1,
    R5C2,
    R5C3,
    R5C4,
    wrong,
    none
}