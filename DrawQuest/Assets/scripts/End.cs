﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class End : MonoBehaviour
{
    [SerializeField]
    Text _waves;
    [SerializeField]
    Button _quitBtn;

    void Start()
    {
        _quitBtn.onClick.AddListener(() => { SceneManage.Instance.LoadScene(1); });
    }

    void OnDestroy()
    {
        _quitBtn.onClick.RemoveAllListeners();
    }

    public void SetWaves(string wave)
    {
        _waves.text = "Wave " + wave;
    }
}
