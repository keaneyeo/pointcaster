﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataAdder : MonoBehaviour
{
    [SerializeField] Transform _waveContentTransform;
    [SerializeField] GameObject _singleWaveUIPrefab;

    [SerializeField] GameObject _waveEditorObject;

    [SerializeField] Button _getAllWavesBtn;
    [SerializeField] Button _addWaveBtn;

	void Start ()
    {
        DataManager.Instance.Connect();
        _getAllWavesBtn.onClick.AddListener(GetAllWaves);
        _addWaveBtn.onClick.AddListener(AddWave);
        Core.SubscribeEvent(Constants.AddWaveDataEvent, AddWaveData);

	}

    private void OnDestroy()
    {
        _getAllWavesBtn.onClick.AddListener(GetAllWaves);
        Core.UnsubscribeEvent(Constants.AddWaveDataEvent, AddWaveData);
    }

    void GetAllWaves()
    {
        ClearContent();
        WaveData[] allwd = DataManager.Instance.GetAllWaveData();
        if (allwd.Length > 0)
        {
            for (int i = 0; i < allwd.Length; i++)
            {
                WaveData wd = allwd[i];
                SingleWaveUI single = Instantiate(_singleWaveUIPrefab, _waveContentTransform).GetComponent<SingleWaveUI>();
                single.Init(wd.wave_id, wd.stage, wd.enemy_type);
            }
        }
        else
        {
            Debug.Log("there is no data!");
        }
    }

    void AddWave()
    {
        WaveEditor editor = Instantiate(_waveEditorObject, transform.parent).GetComponent<WaveEditor>();
    }
    void AddWaveData(object sender, object[] args)
    {
        WaveData wd = new WaveData();
        int stage;
        int.TryParse((string)args[0], out stage);
        wd.stage = stage;
        wd.enemy_type = (int)args[1] + 1;
        DataManager.Instance.AddNewWave(wd);
        Dialog.Instance.Show(GetAllWaves);
        
    }
    void ClearContent()
    {
        int cc = _waveContentTransform.childCount;
        for (int i = 0; i < cc; i++)
        {
            Destroy(_waveContentTransform.GetChild(i).gameObject);
        }
    }
}
