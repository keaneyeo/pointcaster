﻿using UnityEngine;
using UnityEngine.UI;

public class SingleWaveUI : MonoBehaviour
{
    [SerializeField] Text _waveidText;
    [SerializeField] Text _stageText;
    [SerializeField] Text _enemyTypeText;
	
	public bool Init(int waveid, int stagenb, int enemyType)
    {
        _waveidText.text = "Wave_id:" + waveid;
        _stageText.text = "Stage:" + stagenb;
        if (enemyType == 1)
            _enemyTypeText.text = "EnemyType: Normal";
        else
            _enemyTypeText.text = "EnemyType: Boss";
        return true;
    }
}
