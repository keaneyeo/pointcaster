﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;

public class DataManager : MonoBehaviour
{
    static DataManager _instance;
    public static DataManager Instance { get { return _instance; } }

    public static string OverridingConnectionString = null;
    [Multiline(5)]
    public string ConnectionString;
    public DBMSType Type;
    DBNode _dbnode = null;
    private void Awake()
    {
        _instance = this;
        switch (Type)
        {
            case DBMSType.SQLite:
                _dbnode = new DBSQLiteNode(); break;
        }
    }
    public void Connect()
    {
        if(string.IsNullOrEmpty(OverridingConnectionString))
            _dbnode.ConnectToDB(ConnectionString);  
        else
            _dbnode.ConnectToDB(OverridingConnectionString);
    }
    private void OnDestroy()
    {
        _instance = null;
        _dbnode.DisconnectDB();
    }
    
    public WaveData[] GetAllWaveData()
    {
        WaveData[] results = null;
        _dbnode.ExecuteTransaction<WaveData>(delegate (IDbConnection connection,
                                            ref IDataReader reader,
                                            KeyValuePair<string, object>[] args)
        {
            reader = _dbnode.ExecuteWithQuery(DBCommands.GetWaves);
            return reader != null;
        }, ref results, WaveData.Parser);
        return results;
    }

    public WaveData AddNewWave(WaveData wd)
    {
        WaveData[] results = null;
        _dbnode.ExecuteTransaction<WaveData>(delegate (IDbConnection connection,
                                    ref IDataReader reader,
                                    KeyValuePair<string, object>[] args)
        {
            Parameterizer ps = new Parameterizer();
            ps.AddParameter("@stage", wd.stage);
            ps.AddParameter("@enemy_type", wd.enemy_type);
            reader = _dbnode.ExecuteWithQuery(DBCommands.AddWave, ps.Bag.ToArray());
            return reader != null;
        }, ref results, WaveData.Parser);
        if (results != null && results.Length > 0)
            return results[0];
        else
            return null;
    }

    public WaveData EditWave(WaveData wd)
    {
        WaveData[] results = null;
        _dbnode.ExecuteTransaction<WaveData>(delegate (IDbConnection connection,
                                    ref IDataReader reader,
                                    KeyValuePair<string, object>[] args)
        {
            Parameterizer ps = new Parameterizer();
            ps.AddParameter("@stage", wd.stage);
            ps.AddParameter("@enemy_type", wd.enemy_type);
            ps.AddParameter("@wave_id", wd.wave_id);
            reader = _dbnode.ExecuteWithQuery(DBCommands.EditWave, ps.Bag.ToArray());
            return reader != null;
        }, ref results, WaveData.Parser);
        if (results != null && results.Length > 0)
            return results[0];
        else
            return null;
    }

    public Player GetPlayerData()
    {
        Player[] results = null;
        _dbnode.ExecuteTransaction<Player>(delegate (IDbConnection connection,
                                            ref IDataReader reader,
                                            KeyValuePair<string, object>[] args)
        {
            reader = _dbnode.ExecuteWithQuery(DBCommands.GetPlayer);
            return reader != null;
        }, ref results, Player.Parser);
        return results[0];
    }

    public Player EditPlayerData(Player player)
    {
        Player[] results = null;
        _dbnode.ExecuteTransaction<Player>(delegate (IDbConnection connection,
                                            ref IDataReader reader,
                                            KeyValuePair<string, object>[] args)
        {
            Parameterizer ps = new Parameterizer();
            ps.AddParameter("@highest_wave", player.highest_wave);
            ps.AddParameter("@checkpoint", player.checkpoint);
            reader = _dbnode.ExecuteWithQuery(DBCommands.EditPlayerData, ps.Bag.ToArray());
            return reader != null;
        }, ref results, Player.Parser);
        if (results != null && results.Length > 0)
            return results[0];
        else
            return null;
    }

    public PlayerInventory EditPlayerInventoryData(PlayerInventory playerinven)
    {
        PlayerInventory[] results = null;
        _dbnode.ExecuteTransaction<PlayerInventory>(delegate (IDbConnection connection,
                                            ref IDataReader reader,
                                            KeyValuePair<string, object>[] args)
        {
            Parameterizer ps = new Parameterizer();
            ps.AddParameter("@inventory_id", playerinven.inventory_id);
            ps.AddParameter("@skill_id", playerinven.skill_id);
            reader = _dbnode.ExecuteWithQuery(DBCommands.EditPlayerInventoryData, ps.Bag.ToArray());
            return reader != null;
        }, ref results, PlayerInventory.Parser);
        if (results != null && results.Length > 0)
            return results[0];
        else
            return null;
    }

    public Skill EditSkillData(Skill skill)
    {
        Skill[] results = null;
        _dbnode.ExecuteTransaction<Skill>(delegate (IDbConnection connection,
                                            ref IDataReader reader,
                                            KeyValuePair<string, object>[] args)
        {
            Parameterizer ps = new Parameterizer();
            ps.AddParameter("@unlocked", skill.unlocked);
            ps.AddParameter("@skill_id", skill.skill_id);
            reader = _dbnode.ExecuteWithQuery(DBCommands.EditSkillUnlockedData, ps.Bag.ToArray());
            return reader != null;
        }, ref results, Skill.Parser);
        if (results != null && results.Length > 0)
            return results[0];
        else
            return null;
    }

    public EnemyData[] GetAllEnemyData()
    {
        EnemyData[] results = null;
        _dbnode.ExecuteTransaction<EnemyData>(delegate (IDbConnection connection,
                                            ref IDataReader reader,
                                            KeyValuePair<string, object>[] args)
        {
            reader = _dbnode.ExecuteWithQuery(DBCommands.GetEnemies);
            return reader != null;
        }, ref results, EnemyData.Parser);
        return results;
    }

    public Skill[] GetAllSkillsData()
    {
        Skill[] results = null;
        _dbnode.ExecuteTransaction<Skill>(delegate (IDbConnection connection,
                                            ref IDataReader reader,
                                            KeyValuePair<string, object>[] args)
        {
            reader = _dbnode.ExecuteWithQuery(DBCommands.GetSkills);
            return reader != null;
        }, ref results, Skill.Parser);
        return results;
    }

    public PlayerInventory[] GetAllPlayerInventoryData()
    {
        PlayerInventory[] results = null;
        _dbnode.ExecuteTransaction<PlayerInventory>(delegate (IDbConnection connection,
                                            ref IDataReader reader,
                                            KeyValuePair<string, object>[] args)
        {
            reader = _dbnode.ExecuteWithQuery(DBCommands.GetInventory);
            return reader != null;
        }, ref results, PlayerInventory.Parser);
        return results;
    }
}
