﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class WaveEditor : MonoBehaviour
{
    [Header("Stage Editor stuff")]
    [SerializeField] InputField _stageIF;
    [SerializeField] Dropdown _enemyTypeDD;
    [SerializeField] Button _addWaveBtn;

    private void Start()
    {
        _addWaveBtn.onClick.AddListener(() => 
        {
            Core.BroadcastEvent(Constants.AddWaveDataEvent, this,  _stageIF.text, _enemyTypeDD.value);
            Destroy(this.gameObject);
        });
    }
    void OnDestroy()
    {
        _addWaveBtn.onClick.RemoveAllListeners();
    }
    
    //public void Show(UnityAction<int, int> action)
    //{
    //    int i;
    //    int.TryParse(_stageIF.text, out i);
    //    _addWaveBtn.onClick.AddListener(() => 
    //    {
    //        action(i, _enemyTypeDD.value + 1);
    //        Destroy(this.gameObject);
    //    } );
    //}
}
