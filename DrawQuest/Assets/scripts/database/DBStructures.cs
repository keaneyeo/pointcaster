﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;

public static class DBCommands
{
    public const string GetWaves = "select * from wavedata";
    public const string AddWave = "insert into wavedata" +
        "(stage,enemy_type)" +
        "values(@stage,@enemy_type)";

    public const string EditWave = "update wavedata " +
        "set stage=@stage, enemy_type=@enemy_type"+
        "where wave_id=@wave_id";

    public const string GetPlayer = "select * from player";
    public const string GetEnemies = "select * from enemy";
    public const string GetSkills = "select * from skills";
    public const string GetInventory = "select * from player_inventory";

    public const string EditPlayerData = "update player " +
        "set highest_wave=@highest_wave," +
        "checkpoint=@checkpoint";

    public const string EditPlayerInventoryData = "update player_inventory " +
        "set skill_id=@skill_id " +
        "where inventory_id=@inventory_id";

    public const string EditSkillUnlockedData = "update player_inventory " +
      "set unlocked=@unlocked " +
      "where skill_id=@skill_id";
}            
             
public class WaveData
{
    public int wave_id;
    public int stage;
    public int enemy_type;

    public static WaveData Parser(IDataReader reader)
    {
        int nbfields = reader.FieldCount;
        WaveData wd = new WaveData();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "wave_id":
                    wd.wave_id = reader.GetInt32(i);
                    break;
                case "stage":
                    wd.stage = reader.GetInt32(i);
                    break;
                case "enemy_type":
                    wd.enemy_type = reader.GetInt32(i);
                    break;
            }
        }
        return wd;
    }
}

public class EnemyData
{
    public int enemy_id;
    public int health;
    public int attack;
    public int enemy_type;

    public static EnemyData Parser(IDataReader reader)
    {
        int nbfields = reader.FieldCount;
        EnemyData enemy = new EnemyData();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "enemy_id":
                    enemy.enemy_id = reader.GetInt32(i);
                    break;
                case "health":
                    enemy.health = reader.GetInt32(i);
                    break;
                case "attack":
                    enemy.attack = reader.GetInt32(i);
                    break;
                case "enemy_type":
                    enemy.enemy_type = reader.GetInt32(i);
                    break;
            }
        }
        return enemy;
    }
}

public class Player
{
    public int highest_wave;
    public int checkpoint;
    public int health;
    public int attack;


    public static Player Parser(IDataReader reader)
    {
        int nbfields = reader.FieldCount;
        Player player = new Player();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "highest_wave":
                    player.highest_wave = reader.GetInt32(i);
                    break;
                case "checkpoint":
                    player.checkpoint = reader.GetInt32(i);
                    break;
                case "health":
                    player.health = reader.GetInt32(i);
                    break;
                case "attack":
                    player.attack = reader.GetInt32(i);
                    break;
            }
        }
        return player;
    }
}

public class Skill
{
    public int skill_id;
    public int type;
    public string name;
    public int damage;
    public string description;
    public int cooldown;
    public int unlocked;
    public string technical_name;

    public static Skill Parser(IDataReader reader)
    {
        int nbfields = reader.FieldCount;
        Skill skill = new Skill();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "skill_id":
                    skill.skill_id = reader.GetInt32(i);
                    break;
                case "type":
                    skill.type = reader.GetInt32(i);
                    break;
                case "name":
                    skill.name = reader.GetString(i);
                    break;
                case "damage":
                    skill.damage = reader.GetInt32(i);
                    break;
                case "description":
                    skill.description = reader.GetString(i);
                    break;
                case "cooldown":
                    skill.cooldown = reader.GetInt32(i);
                    break;
                case "unlocked":
                    skill.unlocked = reader.GetInt32(i);
                    break;
                case "technical_name":
                    skill.technical_name = reader.GetString(i);
                    break;
            }
        }
        return skill;
    }
}

public class PlayerInventory
{
    public int inventory_id;
    public int skill_id;

    public static PlayerInventory Parser(IDataReader reader)
    {
        int nbfields = reader.FieldCount;
        PlayerInventory playerinventory = new PlayerInventory();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "inventory_id":
                    playerinventory.inventory_id = reader.GetInt32(i);
                    break;
                case "skill_id":
                    playerinventory.skill_id = reader.GetInt32(i);
                    break;
            }
        }
        return playerinventory;
    }
}

public enum EnemyType
{
    None = 0,
    Normal = 1 << 0,
    Boss = 1 << 1
}