﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    static UIManager _instance = null;
    public static UIManager Instance { get { return _instance; } }

    [SerializeField]
    GameObject _confirmation;
    [SerializeField]
    GameObject _loadingScreen;
    [SerializeField]
    GameObject _inventPanel;
    [SerializeField]
    GameObject _settingsPanel;
    [SerializeField]
    GameObject _runeDescription;
    [SerializeField]
    GameObject _helpPanel;
    [SerializeField]
    GameObject _creditsPanel;

    GameObject _tempHolder;

    void Awake()
    {
        if (_instance == null)
            _instance = this;
    }

    void Start()
    {
        Core.SubscribeEvent(Constants.ClosePanel, CloseCurrentPanel);
        Core.SubscribeEvent(Constants.DisplayDesc, DisplayDescription);
    }

    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.ClosePanel, CloseCurrentPanel);
        Core.UnsubscribeEvent(Constants.DisplayDesc, DisplayDescription);
        if (_instance != null)
            _instance = null;
    }

    public void OpenInvent()
    {
        if (_tempHolder != null)
            return;
        if (_tempHolder == null)
            _tempHolder = Instantiate(_inventPanel, this.transform, false);
    }

    public void OpenSettings()
    {
        if (_tempHolder != null)
            return;
        if (_tempHolder == null)
            _tempHolder = Instantiate(_settingsPanel, this.transform, false);
    }

    public void OpenHelp()
    {
        if (_tempHolder != null)
            return;
        if (_tempHolder == null)
            _tempHolder = Instantiate(_helpPanel, this.transform, false);
    }

    public void OpenCredits()
    {
        if (_tempHolder != null)
            return;
        if (_tempHolder == null)
            _tempHolder = Instantiate(_creditsPanel, this.transform, false);
    }
    //public void OpenShop()
    //{

    //}

    //public void OpenNews()
    //{

    //}
    void CloseCurrentPanel(object sender, object[] args)
    {
        if (_tempHolder != null)
            Destroy(_tempHolder.gameObject);
        _tempHolder = null;
    }

    void DisplayDescription(object sender, object[] args)
    {
        GameObject o = Instantiate(_runeDescription, this.transform, false);
        int x = (int)args[0];
        for (int i = 0; i < DataContainer.Skills.Length; i++)
        {
            if(i == x)
            {
                o.GetComponent<RuneDescription>().SetStats(DataContainer.Skills[i]);
                break;
            }
        }
    }

    public void OpenDialog()
    {
        Instantiate(_confirmation, this.transform, false);
    }

    public void CloseDialog(int x)
    {
        int a = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
        if (a == 1 && x == 1)
            OverallManager.Instance.QuitGame();
        else if (a == 2 && x == 1)
            GameStartStop(1);
    }

    public void GameStartStop(int x)
    {
        if (_tempHolder != null)
            return;
        if (_tempHolder == null)
            _tempHolder = Instantiate(_loadingScreen, this.transform, false);
        SceneManage.Instance.LoadScene(x);
    }

    public void CloseLoading()
    {
        if (_tempHolder != null)
            Destroy(_tempHolder);
    }
}
