﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class OverallManager : MonoBehaviour
{
    static OverallManager _instance = null;
    public static OverallManager Instance { get { return _instance; } }

    void Awake ()
    {
        DontDestroyOnLoad(this.gameObject);
        if (_instance == null)
            _instance = this;
        //Debug.Log(Application.persistentDataPath);
        ConfigParser.LoadConfig(Application.persistentDataPath + "/" + "settings.config");
        SetSettings();
#if UNITY_ANDROID && !UNITY_EDITOR
        string dbfile = Application.persistentDataPath + "/rookiesmain.db";
        DataManager.OverridingConnectionString = "URI=file:" + dbfile;
        if (!File.Exists(dbfile))
        {
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/rookiesmain.db");
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
                                        // then save to Application.persistentDataPath
            File.WriteAllBytes(dbfile, loadDb.bytes);
        }

#elif UNITY_IOS
        string dbfile = Application.dataPath + "/Raw/rookiesmain.db";
#else
        string dbfile = Application.dataPath + "/StreamingAssets/rookiesmain.db";
        DataManager.OverridingConnectionString = "URI=file:" + dbfile;
#endif
    }

    void Start()
    {
        DataManager.Instance.Connect();
        DataContainer.GetSkillData();
        DataContainer.GetInventory();
        DataContainer.GetPlayerData();
        DataContainer.GetWaveData();
        SceneManage.Instance.LoadScene(1);
    }

    void SetSettings()
    {
        //if(ConfigParser.SettingsDictionary.ContainsKey("master"))
        //    AudioSettings.Master = ConfigParser.ParseBool(ConfigParser.SettingsDictionary["master"]);
        if (ConfigParser.SettingsDictionary.ContainsKey("bgm"))
            AudioSettings.BGM = ConfigParser.ParseBool(ConfigParser.SettingsDictionary["bgm"]);
        if (ConfigParser.SettingsDictionary.ContainsKey("sfx"))
            AudioSettings.SFX = ConfigParser.ParseBool(ConfigParser.SettingsDictionary["sfx"]);
        //if (ConfigParser.SettingsDictionary.ContainsKey("mastervol"))
        //    AudioSettings.MasterVol = ConfigParser.ParseFloat(ConfigParser.SettingsDictionary["mastervol"]);
        if (ConfigParser.SettingsDictionary.ContainsKey("bgmvol"))
            AudioSettings.BGMVol = ConfigParser.ParseFloat(ConfigParser.SettingsDictionary["bgmvol"]);
        if (ConfigParser.SettingsDictionary.ContainsKey("sfxvol"))
            AudioSettings.SFXVol = ConfigParser.ParseFloat(ConfigParser.SettingsDictionary["sfxvol"]);
    }

    void OnDestroy()
    {
        if (_instance != null)
            _instance = null;
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}