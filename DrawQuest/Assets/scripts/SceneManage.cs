﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManage : MonoBehaviour
{
    static SceneManage _instance = null;
    public static SceneManage Instance { get { return _instance; } }

    AsyncOperation _op;
    bool _loading = false;

    void Awake()
    {
        if (_instance == null)
            _instance = this;
    }

    void OnDestroy()
    {
        if (_instance != null)
            _instance = null;
    }

    public void LoadScene(int x)
    {
        _op = SceneManager.LoadSceneAsync(x);
        _loading = true;
        if (x == 1)
            AudioManager.Instance.SetMusic(LevelType.Main);
        else if (x == 2)
            AudioManager.Instance.SetMusic(LevelType.Game);
    }

    private void Update()
    {
        if (_loading == true)
        {
            if (_op.isDone)
            {
                UIManager.Instance.CloseLoading();
                _loading = false;
            }
        }
    }
}