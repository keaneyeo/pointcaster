1. Pls include the following code.

#if UNITY_ANDROID && !UNITY_EDITOR
        string dbfile = Application.persistentDataPath + "/question.db";
        //if (!File.Exists(dbfile))
        {
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/question.db");
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
                                        // then save to Application.persistentDataPath
            File.WriteAllBytes(dbfile, loadDb.bytes);
        }
#elif UNITY_IOS
        string dbfile = Application.dataPath + "/Raw/question.db";
#else
        string dbfile = Application.dataPath + "/StreamingAssets/question.db";
#endif

2. Change Build Settings > Player Setting > API Compatibility Level from .NET 2.0 Subset to .NET 2.0
3. Place DB file to 'StreamingFolder'. Pls follow cases.